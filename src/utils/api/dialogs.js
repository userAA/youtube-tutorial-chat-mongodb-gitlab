import {axios} from '../../core';

const dialogsApi =  {
    //получаем все диалоги
    getAll: () => {
        return (
            axios.get("/dialogs")
        )
    },
    //создаем диалог
    create: ({partner,text}) => {
        return (
            axios.post("/dialogs",{partner,text})
        )
    },
}

export default dialogsApi;
