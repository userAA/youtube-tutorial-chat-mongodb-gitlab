import {axios} from "../../core";

const userApi = {    
    //авторизация пользователя
    signIn: (postData) => {
        return axios.post ("/user/signin",postData)
    },
    //регистрация пользователя
    signUp: (postData) => {
        return axios.post ("/user/signup",postData)
    },
    //получаем с сервера информацию о авторизованном пользователе
    getMe: () => {
        return axios.get ("/user/me")
    },
    //поиск собеседника
    findUsers: (query) => {
        return axios.get ("/user/find?query=" + query)
    }
}

export default userApi;
