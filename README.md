gitlab page https://gitlab.com/userAA/youtube-tutorial-chat-mongodb-gitlab.git
gitlab youtube-tutorial-chat-mongodb-gitlab

проект youtube_tutorial_chat (достаточно полноценный чат)
используемые технологии на фронтенде:
    @navjobs/upload,
    antd,
    axios,
    classnames,
    date-fns,
    emoji-mart,
    formik,
    identity-obj-proxy,
    lodash,
    nodemon,
    prop-types,
    react,
    react-dom,
    react-redux,
    react-router,
    react-router-dom,
    react-scripts,
    react-string-replace,
    redux,
    redux-thunk,
    sass,
    socket.io-client,
    tinycolor2;

//я такие тестовые данные тут использую
email      test@test.ru
fullname   user1
password   qwerty123
password1  qwerty123

email      test1@test.ru
fullname   user2
password   qwerty1234
password1  qwerty1234

используемые технологии на бекэнд:
    @types/bcrypt,
    @types/dotenv,
    @types/express,
    @types/express-validator,
    @types/jsonwebtoken,
    @types/lodash,
    @types/mongoose,
    @types/multer,
    @types/node,
    @types/passport,
    @types/socket.io,
    @types/validator,
    bcrypt,
    body-parser,
    cloudinary,
    date-fns,
    dotenv,
    express,
    express-validator,
    jsonwebtoken,
    mongoose,
    multer,
    nodemon,
    socket.io,
    ts-node,
    typescript,
    validator;

Имеется авторизация пользователя. Имеется регистрация пользователя.
Сначала регистрируются пользователи. Потом один из них авторизируется и с ним создается диалог, для которого выбирается 
любой неавторизированный пользователь.
Диалог проходит только с двумя пользователями. Пользователей можно сколько угодно создавать.
Диалогов соответственно тоже.
Можно по имени собеседника находить текущий диалог.
Можно переключаться по каждому диалогу между всеми имеющимися.
В каждом диалоге можно помимо стандатных текстовых сообщений, отправлять друг другу картинки звуковые сообщения и текстовые сообщения со смайлами.
В сообщениях есть статус прочитано в виде картинки из двух галочек. Он ставится после того, как собеседник на них ответил.
В противном у сообщений стоит статус чтение из одной галочки.
В каждом диалоге у всех сообщений от автора диалога отмечается время их создания. 
Любое сообщение в каждом диалоге можно удалять.
Если собеседник просматривает сообщения пользователя и на них отвечает, то он тоже в статусе Online. 